**Zadanie 1**
Program ma symulować działanie algorytmów planowania dostępu do procesora dla zgłaszających się procesów.


Zbadać średni czas oczekiwania procesów dla różnych algorytmów planowania:
- FCFS
- SJF(z wywłaszczaniem i bez)
- rotacyjnego (z możliwością wyboru kwantu czasu)


Należy samodzielnie sformułować założenia symulacji.