package dataAnalysis;

public interface ResultMock {
    int[] getResultsArray();
    String getAlgorithmName();
}
