package dataAnalysis;

import java.util.List;

public class ConsoleDisplay {

    private static double roundToTwoDec(double number){
        double x = (double) (Math.round(number * 100));
        x /= 100;
        return x;
    }

    public static void displayAverageTable(List<ResultMock> results){
        String result = "Nazwa algorytmu\tSredni czas\n";

        for (ResultMock element: results){
            result += element.getAlgorithmName() + '\t' + roundToTwoDec(Analysis.getAverage(element.getResultsArray()))  + '\n';
        }
        System.out.println(result);
    }

    public static void displayStDevTable(List<ResultMock> results){
        String result = "Nazwa algorytmu\tOdchylenie stand.\n";

        for (ResultMock element: results){
            result += element.getAlgorithmName() + '\t' + roundToTwoDec(Analysis.getStDev(element.getResultsArray())) + '\n';
        }
        System.out.println(result);
    }

    public static void displayMinTable(List<ResultMock> results){
        String result = "Nazwa algorytmu\tNajkrotszy czas\n";

        for (ResultMock element: results){
            result += element.getAlgorithmName() + '\t' + roundToTwoDec(Analysis.getMin(element.getResultsArray())) + '\n';
        }
        System.out.println(result);
    }

    public static void displayMaxTable(List<ResultMock> results){
        String result = "Nazwa algorytmu\tNajdluzszy czas\n";

        for (ResultMock element: results){
            result += element.getAlgorithmName() + '\t' + roundToTwoDec(Analysis.getMax(element.getResultsArray()))  + '\n';
        }
        System.out.println(result);
    }

    public static void displayAllStats(List<ResultMock> results){
        String result = "Nazwa algorytmu\tNajdl. czas\tNajkr. czas\tSrednia\tOdch. stand.\n";

        for (ResultMock element: results){
            result += element.getAlgorithmName() + '\t' + roundToTwoDec(Analysis.getMax(element.getResultsArray())) +
                    '\t' + roundToTwoDec(Analysis.getMin(element.getResultsArray())) + '\t' +
                    roundToTwoDec(Analysis.getAverage(element.getResultsArray())) + '\t' +
                    roundToTwoDec(Analysis.getStDev(element.getResultsArray())) + '\n';
        }

        System.out.println(result);

    }
}
