package dataAnalysis;

public class Analysis {
	public static double getAverage (int[] data) {
        if (data.length == 0) return 0.0;

    	double result = 0.0;
    	
        for (int element : data)
            result += element;
    	
    	return result / data.length;
    }

    public static double getStDev (int[] data) {
        if (data.length == 0) return 0.0;

    	double result = 0.0;
    	double average = getAverage(data);
    	
    	for (int element : data)
    		result += Math.pow(element - average, 2);
    	
    	return Math.sqrt(result / data.length);
    }

    public static int getMin (int[] data) {
    	int min = Integer.MAX_VALUE;
    	
    	for (int element : data) {
    		min = element < min ? element : min;
    	}
    	
    	return min;
    }

    public static int getMax (int[] data) {
    	int max = Integer.MIN_VALUE;
    	
    	for (int element : data)
    		max = element > max ? element : max;
    	
    	return max;
    }
}
