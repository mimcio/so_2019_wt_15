package main;

import CPU.CPUManager;
import assigner.Assigner;
import assigner.algorithms.AssignerAlgorithm;
import assigner.algorithms.FCFS;
import DataGenerator.Generator;

public class Main {
    private static int time = 0;
    private static int numberOfTasks = 300;

    public static void main(String[] args) {
        CPUManager cpuManager = new CPUManager();

        Generator generator = new Generator(Main.numberOfTasks);
        generator.generateData();
        generator.loadData();

        AssignerAlgorithm algorithm = new FCFS();
        Assigner assigner = new Assigner(generator, cpuManager, algorithm);

        do {
            generator.tick();
            assigner.tick();
            cpuManager.tick();

            Main.time++;
        } while (!generator.allTasksFinished());
    }

    public static int getTime() {
        return Main.time;
    }
}
