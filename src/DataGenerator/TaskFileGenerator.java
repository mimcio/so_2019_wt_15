package DataGenerator;

import java.util.ArrayList;
import java.util.Random;

public class TaskFileGenerator {
    private int time;
    private int lastId;
    private int NumberOfTasks;

    private final static int maxTime = 30;

    public TaskFileGenerator(int number) {
        this.time = 1;
        this.lastId = 6;
        this.NumberOfTasks = number;
    }

    public ArrayList<Task> generateArrayOfTasks() {

        ArrayList<Task> list = new ArrayList<>();
        Random rand = new Random();

        list.add(new Task(20, 1, 0));
        list.add(new Task(46, 2, 0));
        list.add(new Task(85, 3, 0));
        list.add(new Task(5, 4, 0));
        list.add(new Task(10, 5, 0));

        while(this.lastId <= this.NumberOfTasks) {

            if(time % 3 == 0) {

                int howMany = rand.nextInt(3);

                for (int i = 0; i < howMany; i++) {
                    list.add(this.addTask());

                }
            } 

            this.time++;
        }

        return list;
    }

    private Task addTask() {

        Random rand = new Random();
        Task temp = new Task(rand.nextInt(TaskFileGenerator.maxTime) + 1, this.lastId, this.time);

        this.lastId++;

        return temp;
    }
}