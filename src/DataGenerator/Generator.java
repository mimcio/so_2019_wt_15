package DataGenerator;

import main.Main;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class Generator {
    private int numberOfTasks;
    private int taskCounter;

    private ArrayList<Task> ourList;
    private ArrayList<Task> publicList;

    private FileOperations fileOperations;

    public Generator(int numberOfTasks) {
        this.numberOfTasks = numberOfTasks;
        this.taskCounter = 0;

        this.publicList = new ArrayList<>();                //uwtórz tablice prywatna i publiczna

        this.fileOperations = new FileOperations();         //utwórz obiekt do wczytywania i zapisywania
    }

    public ArrayList<Task> getArrayListProcesses() {
        return this.publicList;
    }

    public boolean allTasksFinished() {
        for (int i = this.ourList.size()-1; i >= 0; i--)
             if (!this.ourList.get(i).isFinished()) 
                return false;

        return true;

    }

    public void tick() {
        this.checkNewTasks();
    }

    public void checkNewTasks() {

        if (this.taskCounter > this.numberOfTasks || this.allTasksFinished())
            return;

        ArrayList<Task> tasksToAdd = new ArrayList<>(this.ourList.stream()
                .filter(task -> !task.isFinished())
                .filter(task -> Main.getTime() >= task.getCreationTime())
                .collect(Collectors.toList()));

        this.taskCounter += tasksToAdd.size();
        this.ourList = tasksToAdd;
    }

    public void generateData() {
        TaskFileGenerator generator = new TaskFileGenerator(this.numberOfTasks);
        this.fileOperations.saveArray(generator.generateArrayOfTasks());        //zapisz liste procesow
    }

    public void loadData() {
        this.ourList = this.fileOperations.loadArray();                         //wczytaj liste procesów
    }

}