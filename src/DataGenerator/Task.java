package DataGenerator;

import java.io.Serializable;

import main.Main;

public class Task implements Serializable{

    private long executionTime;
    private long creationTime;
    private long waitingTime;
    private long taskID;
    private long finishTime;

    public Task (long executionTime, long taskID, long creationTime) {
        this.executionTime = executionTime;
        this.taskID = taskID;
        this.creationTime = creationTime;
        this.waitingTime = 0;
        this.finishTime = 0;
    }

    //get'ery

    public long getExecuteTime() {
        return this.executionTime;
    }

    public long getTaskID() {
        return this.taskID;
    }

    public long getCreationTime() {
        return this.creationTime;
    }

    public boolean isFinished() { return this.finishTime > 0; }

    public void setFinished() { this.finishTime = Main.getTime(); }
}