package CPU;

import DataGenerator.Task;

import java.util.ArrayList;

public class CPUManager{

     ArrayList<Processor> listCPU;


     public CPUManager(){
        listCPU = new ArrayList<>();

     }

     public void tick(){
        for(Processor proc : listCPU){

        }
     }

     public void accept(ArrayList<Task> listTask){
         for(int i=0;i<listTask.size();i++){
             for(Processor proc : listCPU){
                 if(proc.getFree()){
                     proc.setTask(listTask.get(i));
                     break;
                 }
             }
         }
     }

     public int getAvailableCount(){
         int avalibleProcessors=0;
        for(Processor proc : listCPU){
            if(proc.getFree())
                avalibleProcessors++;
        }
     }



}