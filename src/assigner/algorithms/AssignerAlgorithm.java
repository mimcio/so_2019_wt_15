package assigner.algorithms;

import DataGenerator.Task;

import java.util.ArrayList;

public interface AssignerAlgorithm {
    ArrayList<Task> getNext(ArrayList<Task> tasks, int count);
}
