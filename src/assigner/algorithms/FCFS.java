package assigner.algorithms;

import DataGenerator.Task;
import java.util.ArrayList;

public class FCFS implements AssignerAlgorithm {

    @Override
    public ArrayList<Task> getNext(ArrayList<Task> tasks, int count) {
        ArrayList<Task> result = new ArrayList<>();
        for (int i = 0; i < count && tasks.size() > 0; i++) {
            result.add(tasks.remove(0));
        }
        return result;
    }
}
