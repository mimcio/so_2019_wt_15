package assigner;

import assigner.algorithms.AssignerAlgorithm;
import DataGenerator.Task;
import DataGenerator.Generator;

import CPU.CPUManager;

import java.util.ArrayList;

public class Assigner {
    ArrayList<Task> waiting;
    AssignerAlgorithm assignerAlgorithm;
    CPUManager cpuManager;
    Generator generator;

    public Assigner(Generator generator, CPUManager cpuManager, AssignerAlgorithm algorithm) {
        this.cpuManager = cpuManager;
        this.assignerAlgorithm = algorithm;
        this.generator = generator;
    }

    public void tick() {
        waiting.addAll(generator.getArrayListProcesses());
        cpuManager.accept(assignerAlgorithm.getNext(waiting, cpuManager.getAvailableCount()));
    }
}
